import React, { Component } from "react";
import { dataGlasses } from "./DataGlasses";

export default class ShopGlasses extends Component {
  state = {
    data: {},
  };
  renderListGlasses = () => {
    return dataGlasses.map((item, index) => {
      return (
        <button
          key={index}
          onClick={() => {
            this.handleChange(item);
          }}
          className="btn"
        >
          <img style={{ width: "100%" }} src={item.src} alt="" />
        </button>
      );
    });
  };
  renderModel = () => {
    return (
      <div className="model row">
        <div className="model1 col-6">
          <img src="./glassesImage/model.jpg" alt="" />
        </div>
        <div className="model2 col-6">
          <img src="./glassesImage/model.jpg" alt="" />
        </div>
      </div>
    );
  };
  handleChange = (list) => {
    this.setState({
      data: list,
      class: "info",
    });
  };
  render() {
    return (
      <div>
        <div className="header">
          <h2>TRY GLASSES APP ONLINE</h2>
        </div>
        <div className="image">
          <img src={this.state.data.url} alt="" />
        </div>
        <div className={this.state.class}>
          <h5 className="text-primary">{this.state.data.name}</h5>
          <p className="text-white">{this.state.data.desc}</p>
        </div>
        {this.renderModel()}
        <div className="listGlasses d-flex container bg-white">
          {this.renderListGlasses()}
        </div>
      </div>
    );
  }
}
